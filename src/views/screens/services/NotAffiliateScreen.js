import React, {Component} from 'react';
import {View, StyleSheet} from 'react-native';
import {Button, SubtitleText, TitleText} from '../../widgets';

class NotAffiliateScreen extends Component {
  render() {
    const {navigation} = this.props;

    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <TitleText text="Hola Usuario" />
          <SubtitleText text="Te damos la bienvenida" />
        </View>
        <View style={styles.body}>
          <SubtitleText text="Aún no estas afiliado" style={{marginBottom: 20}} />
          <Button
            type="primary"
            title="¡Afiliarse ahora!"
            containerStyle={{width: 200, marginVertical: 10}}
            onPress={() => navigation.navigate('Dashboard')}
          />
          <Button
            type="secondary"
            title="¡Quiero cotizar!"
            containerStyle={{width: 200, marginVertical: 10}}
            onPress={() => navigation.navigate('Dashboard')}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
   flex: 1,
  },
  header: {
    paddingHorizontal: 21,
    paddingVertical: 35,
    backgroundColor: '#fff',
    borderBottomWidth: 1,
    borderBottomColor: 'rgb(230, 235, 241)',
  },
  body: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
});

export default NotAffiliateScreen;