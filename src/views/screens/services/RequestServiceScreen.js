import React, {Component} from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';
import {Card, ScrollContainer, SubtitleText, TitleText} from '../../widgets';

const teleMedicine = require('../../../assets/images/teleMedicina.png');

const listServices = [
  {name: 'Telemedicina', screen: 'Telemedicine'},
  {name: 'Atención médica domiciliaria', screen: ''},
  {name: 'Entrega de medicamentos', screen: ''},
  {name: 'Ambulancias', screen: ''},
  {name: 'Hospitalización de emergencia', screen: ''},
  {name: 'Consulta médica por especialista', screen: ''},
];

class RequestServiceScreen extends Component {
  render() {
    const {navigation} = this.props;

    return (
      <View>
        <TitleText text="¿Cuál servicio?" style={{marginTop: 21, marginLeft: 21, marginBottom: 0}} />
        <ScrollContainer
          contentContainerStyle={{
            backgroundColor: 'transparent',
            flexWrap: 'wrap',
            flexDirection: 'row',
          }}
        >
          {listServices.map((s, i) => (
            <TouchableOpacity
              key={i}
              style={{width: '50%', padding: 10}}
              onPress={() => navigation.navigate(s.screen)}
            >
              <Card style={{alignItems: 'center', flex: 1}}>
                <Image
                  style={{width: 53, height: 53}}
                  source={teleMedicine}
                />
                <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                  <SubtitleText text={s.name} style={{textAlign: 'center', marginVertical: 10}} />
                </View>
              </Card>
            </TouchableOpacity>
          ))}
        </ScrollContainer>
      </View>
    );
  }
}

export default RequestServiceScreen;