import React, {Component} from 'react';
import {Text, View, StyleSheet, TouchableOpacity, ScrollView} from 'react-native';
import {Button, Container, ScrollContainer, SubtitleText} from '../../widgets';
import {DISABLED_COLOR, PRIMARY_COLOR} from '../../../utils/Constants';
import ServiceCard from '../../components/ServiceCard';

const services = [
  {
    title: 'Telemedicina',
    status: 'pending',
    body: {
      dateInit: '12/03/2020 14:30',
      dateEnd: '12/03/2020 14:30',
      attendedBy: 'Dr. Jose Perez',
      symptoms: 'Dolor de cabeza, malestar, fiebre, asfixia nocturna',
    },
  },
  {
    title: 'Telemedicina',
    status: 'inProgress',
    body: {
      dateInit: '12/03/2020 14:30',
      dateEnd: '12/03/2020 14:30',
      attendedBy: 'Dr. Jose Perez',
      symptoms: 'Dolor de cabeza, malestar, fiebre, asfixia nocturna',
    },
  },
  {
    title: 'Telemedicina',
    status: 'completed',
    body: {
      dateInit: '12/03/2020 14:30',
      dateEnd: '12/03/2020 14:30',
      attendedBy: 'Dr. Jose Perez',
      symptoms: 'Dolor de cabeza, malestar, fiebre, asfixia nocturna',
    },
  },
];

class DashboardScreen extends Component {
  state = {
    currentTab: 0,
    pending: 1,
    inProgress: 0,
    completed: 0,
  };

  onChangeTab = (value) => {
    this.setState({currentTab: value});
  };

  render () {
    const {
      currentTab,
      pending,
    } = this.state;

    return (
      <View style={styles.container}>
        <View style={styles.tabContainer}>
          <TouchableOpacity
            style={[styles.tab, styles[currentTab === 0 ? 'tabSelected' : '']]}
            onPress={() => this.onChangeTab(0)}
          >
            <Text style={[styles.tabText, styles[currentTab === 0 ? 'selected' : 'noSelected']]}>Pendientes</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.tab, styles[currentTab === 1 ? 'tabSelected' : '']]}
            onPress={() => this.onChangeTab(1)}
          >
            <Text style={[styles.tabText, styles[currentTab === 1 ? 'selected' : 'noSelected']]}>En curso</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.tab, styles[currentTab === 2 ? 'tabSelected' : '']]}
            onPress={() => this.onChangeTab(2)}
          >
            <Text style={[styles.tabText, styles[currentTab === 2 ? 'selected' : 'noSelected']]}>Finalizados</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.tabBody}>
          {currentTab === 0 && (
            <ScrollContainer contentContainerStyle={{backgroundColor: 'transparent'}}>
              <View style={styles.tabBodyContainer}>
                {pending === 0
                  ? (
                    <View style={styles.noContent}>
                      <SubtitleText text="No tienes servicios pendientes" style={{marginBottom: 40}}/>
                      <Button type="primary" title="Solicitar servicio" containerStyle={{width: 200}}/>
                    </View>
                  ) : (
                      services.map((s, i) => (
                        <ServiceCard
                          key={i}
                          title={s.title}
                          body={s.body}
                          status={s.status}
                        />
                      ))
                  )}
              </View>
            </ScrollContainer>
          )}
          {currentTab === 1 && (
            <Container style={{backgroundColor: 'transparent'}}>
              <View style={styles.tabBodyContainer}>
                <View style={styles.noContent}>
                  <SubtitleText text="No tienes servicios en curso" style={{marginBottom: 40}}/>
                  <Button type="primary" title="Solicitar servicio" containerStyle={{width: 200}}/>
                </View>
              </View>
            </Container>
          )}
          {currentTab === 2 && (
            <Container style={{backgroundColor: 'transparent'}}>
              <View style={styles.tabBodyContainer}>
                <View style={styles.noContent}>
                  <SubtitleText text="No tienes servicios finalizados" style={{marginBottom: 40}}/>
                  <Button type="primary" title="Solicitar servicio" containerStyle={{width: 200}}/>
                </View>
              </View>
            </Container>
          )}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  tabContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 70,
    borderBottomWidth: 1,
    borderBottomColor: 'rgb(228, 233, 242)',
    backgroundColor: '#fff'
  },
  tabBody: {
    flex: 1,
  },
  tab: {
    height: 70,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  tabText: {
    fontSize: 18
  },
  tabSelected: {
    borderBottomWidth: 2,
    borderBottomColor: PRIMARY_COLOR,
  },
  selected: {
    color: PRIMARY_COLOR,
    fontWeight: 'bold',
  },
  noSelected: {
    color: DISABLED_COLOR,
  },
  tabBodyContainer: {
    flex: 1,
    // alignItems: 'center',
  },
  noContent: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default DashboardScreen;