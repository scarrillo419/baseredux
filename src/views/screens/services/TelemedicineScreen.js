import React, {Component} from 'react';
import {View, Text, Image, StyleSheet} from 'react-native';
import {Button, InputText, ScrollContainer} from '../../widgets';

const videoCall = require('../../../assets/images/videoCall.png');

class TelemedicineScreen extends Component {
  render() {
    return (
      <ScrollContainer>
        <View style={styles.alertContainer}>
          <Image source={videoCall} style={styles.image} />
          <Text style={styles.text}>Establece conexión inmediata con nuestros médicos a través de videollamada.</Text>
        </View>
        <InputText
          label="Motivo"
          placeholder="Indicar motivo"
          multiline
        />
        <InputText
          label="Síntomas"
          placeholder="¿Cuáles son tus síntomas?"
          multiline
        />
        <Button
          title="¡Solicitar!"
          type="primary"
          containerStyle={{flex: 1, justifyContent: 'flex-end'}}
        />
      </ScrollContainer>
    );
  }
}

const styles = StyleSheet.create({
  alertContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 5,
    borderWidth: 2,
    borderColor: 'rgb(32, 8, 68)',
    backgroundColor: 'rgb(241, 232, 255)',
    padding: 10,
    marginBottom: 30,
  },
  image: {
    width: 32,
    height: 32,
    // padding: 20,
  },
  text: {
    flex: 1,
    fontSize: 18,
    color: 'rgb(32, 8, 68)',
    padding: 10,
  },
});

export default TelemedicineScreen;