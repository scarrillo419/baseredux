import React, {Component} from 'react';
import {
  Button,
  InputText,
  RadioButton,
  ScrollContainer,
  TitleText,
} from '../../widgets';
import {SignupValidation} from '../../../utils/validations';

class SignupScreen extends Component {
  state = {
    isValid: false,
    signupForm: {
      email: '',
      password: '',
      confirmPassword: '',
      policy: false,
    },
    formErrors: {},
    showModal: false,
  };

  handleInput = (name, text) => {
    const {signupForm} = this.state;
    signupForm[name] = text;
    this.setState({signupForm}, async () => await this.validateFormInput(name, text));
  };

  validateFormInput = async (name, text) => {
    const {signupForm, formErrors} = this.state;
    await SignupValidation.validate(signupForm, {strict: true, abortEarly: false})
      .then(() => delete formErrors[name])
      .catch(error => formErrors[name] = error.errors[0][name])
      .finally(async () => {
        this.setState({formErrors})
        await this.validateForm();
      });
  };

  validateForm = async () => {
    const {signupForm} = this.state;
    const isValid = await SignupValidation.isValid(signupForm);
    this.setState({isValid});
  };

  render() {
    const {navigation} = this.props;

    const {
      isValid,
      signupForm,
      formErrors,
    } = this.state;

    return (
      <ScrollContainer>
        <TitleText text="¡Únete ahora!" />

        <InputText
          name="email"
          value={signupForm.email}
          error={formErrors.email}
          placeholder="Correo eletrónico"
          autoCapitalize="none"
          onChangeText={(e) => this.handleInput('email', e)}
        />
        <InputText
          name="password"
          value={signupForm.password}
          error={formErrors.password}
          placeholder="Contraseña"
          secure
          showPassword
          autoCapitalize="none"
          onChangeText={(e) => this.handleInput('password', e)}
        />
        <InputText
          name="confirmPassword"
          value={signupForm.confirmPassword}
          error={formErrors.confirmPassword}
          placeholder="Confirmar contraseña"
          secure
          showPassword
          autoCapitalize="none"
          onChangeText={(e) => this.handleInput('confirmPassword', e)}
        />
        <RadioButton
          name="policy"
          label="Acepto las Politicas de privacidad"
          value={signupForm.policy}
          containerStyle={{marginVertical: 20}}
          onChange={(e) => this.handleInput('policy', e)}
        />
        <Button
          title="Crear cuenta"
          disabled={!isValid}
          type="primary"
          containerStyle={{flex: 1, justifyContent: 'flex-end'}}
        />
      </ScrollContainer>
    )
  }
}

export default SignupScreen;