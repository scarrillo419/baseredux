import React, {Component} from 'react';
import {Button, InputText, ScrollContainer, TitleText} from '../../widgets';
import {RestoreValidation} from '../../../utils/validations';

class RestorePasswordScreen extends Component {
  state = {
    isValid: false,
    restoreForm: {
      email: '',
    },
    formErrors: {},
  };

  handleInput = (name, text) => {
    const {restoreForm} = this.state;
    restoreForm[name] = text;
    this.setState({restoreForm}, async () => await this.validateFormInput(name, text));
  };

  validateFormInput = async (name, text) => {
    const {restoreForm, formErrors} = this.state;
    await RestoreValidation.validate(restoreForm, {strict: true, abortEarly: false})
      .then(() => delete formErrors[name])
      .catch(error => formErrors[name] = error.errors[0][name])
      .finally(async () => {
        this.setState({formErrors})
        await this.validateForm();
      });
  }

  validateForm = async () => {
    const {restoreForm} = this.state;
    const isValid = await RestoreValidation.isValid(restoreForm);
    this.setState({isValid});
  };

  render() {
    const {navigation} = this.props;

    const {
      isValid,
      restoreForm,
      formErrors,
    } = this.state;

    return (
      <ScrollContainer>
        <TitleText text="Restablecer la contraseña" />
        <InputText
          name="email"
          value={restoreForm.email}
          error={formErrors.email}
          placeholder="Correo eletrónico"
          autoCapitalize="none"
          style={{fontWeight: 'bold'}}
          onChangeText={(e) => this.handleInput('email', e)}
        />
        <Button
          title="Restablecer"
          disabled={!isValid}
          type="primary"
          containerStyle={{flex: 1, justifyContent: 'flex-end'}}
          onPress={() => navigation.navigate('RestoreSuccess')}
        />
      </ScrollContainer>
    );
  }
}

export default RestorePasswordScreen;