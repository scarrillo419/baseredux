import React, {Component} from 'react';
import {Image, Text, StyleSheet} from 'react-native';
import {Button, Container} from '../../widgets';
import {SECONDARY_COLOR} from '../../../utils/Constants';

class RestoreSuccessScreen extends Component {
  render() {
    const {navigation} = this.props;

    return (
      <Container style={{backgroundColor: SECONDARY_COLOR}}>
        <Image
          source={require('../../../assets/images/ilustracionMail.png')}
          style={styles.image}
        />
        <Text style={styles.text}>Te hemos enviado un correo electrónico con el enlace para restablecer la contraseña</Text>
        <Button
          title="Entendido"
          type="default"
          containerStyle={{flex: 1, justifyContent: 'flex-end'}}
          onPress={() => navigation.navigate('Signin')}
        />
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  image: {
    width: 104,
    height: 106,
    marginVertical: 50,
    alignSelf: 'center'
  },
  text: {
    color: 'white',
    fontSize: 24,
    lineHeight: 36,
    textAlign: 'center',
    paddingHorizontal: 40,
  }
});

export default RestoreSuccessScreen;