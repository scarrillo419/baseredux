import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  InputText,
  Button,
  TitleText,
  LinkText,
  ScrollContainer,
  InfoModal,
} from '../../widgets';
import {SigninValidation} from '../../../utils/validations';
import {signin} from '../../../store/auth/actions';

class SigninScreen extends Component {
  state = {
    isValid: false,
    signinForm: {
      email: '',
      password: '',
    },
    formErrors: {},
    showModal: false,
  };

  componentDidMount() {
    this.setState({showModal: true});
  }

  handleInput = (name, text) => {
    const {signinForm} = this.state;
    signinForm[name] = text;
    this.setState({signinForm}, async () => await this.validateFormInput(name, text));
  };

  validateFormInput = async (name, text) => {
    const {signinForm, formErrors} = this.state;
    await SigninValidation.validate(signinForm, {strict: true, abortEarly: false})
      .then(() => delete formErrors[name])
      .catch(error => formErrors[name] = error.errors[0][name])
      .finally(async () => {
        this.setState({formErrors})
        await this.validateForm();
      });
  };

  validateForm = async () => {
    const {signinForm} = this.state;
    const isValid = await SigninValidation.isValid(signinForm);
    this.setState({isValid});
  };

  toggleModal = () => {
    const {showModal} = this.state;
    this.setState({showModal: !showModal});
  }

  submit = () => {
    // const {signinForm} = this.state;
    // this.props.signin(signinForm);
    this.props.navigation.navigate('App');
  };

  render() {
    const {
      navigation,
      loading,
    } = this.props;

    const {
      isValid,
      signinForm,
      formErrors,
      showModal,
    } = this.state;

    return (
      <ScrollContainer>
        <TitleText text="¡Ingresa ahora!" />

        <InputText
          name="email"
          value={signinForm.email}
          error={formErrors.email}
          placeholder="Correo eletrónico"
          autoCapitalize="none"
          onChangeText={(e) => this.handleInput('email', e)}
        />
        <InputText
          name="email"
          value={signinForm.email}
          error={formErrors.email}
          placeholder="Correo eletrónico"
          autoCapitalize="none"
          onChangeText={(e) => this.handleInput('email', e)}
        />
        <InputText
          name="email"
          value={signinForm.email}
          error={formErrors.email}
          placeholder="Correo eletrónico"
          autoCapitalize="none"
          onChangeText={(e) => this.handleInput('email', e)}
        />
        <InputText
          name="email"
          value={signinForm.email}
          error={formErrors.email}
          placeholder="Correo eletrónico"
          autoCapitalize="none"
          onChangeText={(e) => this.handleInput('email', e)}
        />
        <InputText
          name="email"
          value={signinForm.email}
          error={formErrors.email}
          placeholder="Correo eletrónico"
          autoCapitalize="none"
          onChangeText={(e) => this.handleInput('email', e)}
        />
        <InputText
          name="email"
          value={signinForm.email}
          error={formErrors.email}
          placeholder="Correo eletrónico"
          autoCapitalize="none"
          onChangeText={(e) => this.handleInput('email', e)}
        />
        <InputText
          name="email"
          value={signinForm.email}
          error={formErrors.email}
          placeholder="Correo eletrónico"
          autoCapitalize="none"
          onChangeText={(e) => this.handleInput('email', e)}
        />
        <InputText
          name="email"
          value={signinForm.email}
          error={formErrors.email}
          placeholder="Correo eletrónico"
          autoCapitalize="none"
          onChangeText={(e) => this.handleInput('email', e)}
        />
        <InputText
          name="email"
          value={signinForm.email}
          error={formErrors.email}
          placeholder="Correo eletrónico"
          autoCapitalize="none"
          onChangeText={(e) => this.handleInput('email', e)}
        />
        <InputText
          name="email"
          value={signinForm.email}
          error={formErrors.email}
          placeholder="Correo eletrónico"
          autoCapitalize="none"
          onChangeText={(e) => this.handleInput('email', e)}
        />
        <InputText
          name="email"
          value={signinForm.email}
          error={formErrors.email}
          placeholder="Correo eletrónico"
          autoCapitalize="none"
          onChangeText={(e) => this.handleInput('email', e)}
        />
        <InputText
          name="password"
          value={signinForm.password}
          error={formErrors.password}
          placeholder="Contraseña"
          secure
          showPassword
          autoCapitalize="none"
          onChangeText={(e) => this.handleInput('password', e)}
        />
        <LinkText
          text="¿Olvidaste tu contraseña?"
          align="right"
          containerStyle={{marginTop: 10, marginBottom: 20}}
          onPress={() => navigation.navigate('Restore')}
        />
        <Button
          title="Iniciar sesión"
          disabled={!isValid}
          type="primary"
          containerStyle={{flex: 1, justifyContent: 'flex-end'}}
          loading={loading}
          onPress={this.submit}
        />
        <InfoModal
          visible={showModal}
          animationType='slide'
          onClose={this.toggleModal}
          title="Información"
          bodyText="Si ya te registraste puedes ingresar de nuevo con tu usuario y contraseña. Si eres beneficiario (alguien más te afilió) debes ingresar con tu email y la contraseña que te enviamos a tu correo electrónico al momento de la afiliación."
        />
      </ScrollContainer>
    );
  }
}

const mapStateToProps = state => ({
  loading: state.auth.loading,
});

const mapDispatchToProps = dispatch => ({
  signin: (data) => dispatch(signin(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SigninScreen);