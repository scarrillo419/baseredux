import React, {Component} from 'react';
import {Container, SubtitleText, TitleText} from '../widgets';

class ServicesScreen extends Component {
  render() {
    return (
      <Container>
        <TitleText text="Bienvenido Usuario" />
        <SubtitleText text="Te damos la bienvenida" />
      </Container>
    );
  }
}

export default ServicesScreen;