import React, {Component} from 'react';
import {View, StyleSheet} from 'react-native';
import Slides from '../components/Slides';
import {Button} from '../widgets';
import {BACKGROUND_CONTAINER_COLOR} from '../../utils/Constants';

const DATA = [
  {
    key: 0,
    title: '¿Qué es asistensi?',
    subtitle: 'Es el mejor seguro para emergencias médicas con atención médica inmediata.',
    image: require('../../assets/images/welcome/imagen1.jpg'),
  },
  {
    key: 1,
    title: '¡Afilia a tu familia!',
    subtitle: 'Proteger a tu familia nunca fue tan fácil. Cuida a tus seres queridos en su país de origen.',
    image: require('../../assets/images/welcome/imagen2.jpg'),
  },
  {
    key: 2,
    title: 'Beneficios',
    subtitle: 'Telemedicina, atención domiciliaria, medicamentos, ambulancias, hospitalización, entre otros.',
    image: require('../../assets/images/welcome/imagen3.jpg'),
  },
];

class WelcomeScreen extends Component {
  render() {
    const {navigation} = this.props;

    return (
      <View style={styles.container}>
        <View style={styles.slides}>
          <Slides data={DATA} />
        </View>
        <View style={styles.groupButtons}>
          <View style={styles.button}>
            <Button
              title="Ingreso"
              type="secondary"
              onPress={() => navigation.navigate('Signin')}
            />
          </View>
          <View style={styles.button}>
            <Button
              title="Registro"
              type="primary"
              onPress={() => navigation.navigate('Signup')}
            />
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    backgroundColor: BACKGROUND_CONTAINER_COLOR
  },
  slides: {
    flex: 1,
  },
  groupButtons: {
    paddingVertical: 20,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  button: {
    flex: 1,
    marginHorizontal: 10,
  }
});

export default WelcomeScreen;