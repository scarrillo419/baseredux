import React, {Component} from 'react';
import {StyleSheet, ScrollView, Platform, KeyboardAvoidingView} from 'react-native';
import {BACKGROUND_CONTAINER_COLOR, PADDING_GENERAL_CONTAINER} from '../../utils/Constants';

class ScrollContainer extends Component {
  render() {
    const {
      children,
      keyboardShouldPersistTaps = 'always',
      style = {},
      contentContainerStyle = {}
    } = this.props;

    return (
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
        style={{ flexGrow: 1 }}
      >
        <ScrollView
          style={style}
          contentContainerStyle={[styles.container, contentContainerStyle]}
          keyboardShouldPersistTaps={keyboardShouldPersistTaps}
        >
          {children}
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    padding: PADDING_GENERAL_CONTAINER,
    backgroundColor: BACKGROUND_CONTAINER_COLOR,
  }
});

export {ScrollContainer};