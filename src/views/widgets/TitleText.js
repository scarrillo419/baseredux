import React, {Component} from 'react';
import {Text, StyleSheet} from 'react-native';
import {SECONDARY_COLOR} from '../../utils/Constants';

class TitleText extends Component {
  render() {
    const {
      text,
      style = {},
    } = this.props;

    return <Text style={[styles.text, style]}>{text}</Text>;
  }
}

const styles = StyleSheet.create({
  text: {
    fontSize: 40,
    marginBottom: 27,
    lineHeight: 53,
    color: SECONDARY_COLOR,
    fontWeight: 'bold'
  }
});

export {TitleText};