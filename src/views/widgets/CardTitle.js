import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';

class CardTitle extends Component {
  render() {
    const {children} = this.props;

    return (
      <View style={styles.container}>
        <Text style={styles.text}>{children}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    padding: 15,
    borderBottomWidth: 1,
    borderBottomColor: '#c9c9c9',
  },
  text: {
    fontSize: 24,
    color: '#555555',
    fontWeight: '700',
  },
});

export {CardTitle};