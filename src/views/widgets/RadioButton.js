import React, {Component} from 'react';
import {View, Text, Image, StyleSheet, TouchableOpacity} from 'react-native';

const radioButton = require('../../assets/images/radioButton.png');
const radioButtonChecked = require('../../assets/images/radioButtonChecked.png');

class RadioButton extends Component {
  onChangeValue = () => {
    const {onChange, value} = this.props;
    onChange(!value);
  };

  render() {
    const {
      value,
      label,
      containerStyle = {},
    } = this.props;


    return (
      <TouchableOpacity
        onPress={this.onChangeValue}
        style={[styles.container, containerStyle]}
      >
        <Image
          style={styles.checkBox}
          source={value ? radioButtonChecked : radioButton}
        />
        <Text style={styles.label}>{label}</Text>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  checkBox: {
    width: 32,
    height: 32,
    marginRight: 5,
  },
  label: {
    fontSize: 18,
    color: 'rgb(67, 72, 77)',
  }
})

export {RadioButton};