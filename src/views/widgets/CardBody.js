import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';

class CardBody extends Component {
  render() {
    const {children} = this.props;

    return (
      <View style={styles.container}>
        {children}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    padding: 15,
  },
});

export {CardBody};