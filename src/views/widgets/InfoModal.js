import React, {Component} from 'react';
import {Modal, View, Text, StyleSheet} from 'react-native';
import {Button} from './Button';
import {SECONDARY_COLOR} from '../../utils/Constants';

class InfoModal extends Component {
  render() {
    const {
      visible = false,
      onClose,
      title,
      bodyText,
    } = this.props;

    return (
      <Modal
        visible={visible}
        transparent={true}
        statusBarTranslucent
        animationType="fade"
      >
        <View style={styles.container}>
          <View style={styles.contentModal}>
            <View style={styles.title}>
              <Text style={styles.titleText}>{title}</Text>
            </View>
            <View style={styles.body}>
              <Text style={styles.bodyText}>{bodyText}</Text>
            </View>
            <View style={styles.footer}>
              <Button type="secondary" title="Entendido" onPress={onClose} />
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    flex: 1,
    justifyContent: 'flex-end',
  },
  contentModal: {
    borderTopStartRadius: 8,
    borderTopEndRadius: 8,
    backgroundColor: 'white',
    padding: 21,
    justifyContent: 'space-between'
  },
  body: {
    flexGrow: 1,
    marginVertical: 21,
  },
  footer: {
  },
  titleText: {
    color: SECONDARY_COLOR,
    fontSize: 26,
    lineHeight: 40,
  },
  bodyText: {
    fontSize: 21,
    lineHeight: 34
  },
});

export {InfoModal};