import React, {Component} from 'react';
import {Text, TouchableOpacity, StyleSheet} from 'react-native';
import {LINK_COLOR} from '../../utils/Constants';

class LinkText extends Component {
  render() {
    const {
      text,
      containerStyle = {},
      textStyle = {},
      align = 'left',
      onPress,
    } = this.props;

    return (
      <TouchableOpacity onPress={onPress} style={containerStyle}>
        <Text style={[styles.text, styles[`${align}Align`], textStyle]}>{text}</Text>
      </TouchableOpacity>
    )
  }
}

const styles = StyleSheet.create({
  text: {
    color: LINK_COLOR,
    fontSize: 18,
  },
  leftAlign: {
    textAlign: 'left',
  },
  centerAlign: {
    textAlign: 'center',
  },
  rightAlign: {
    textAlign: 'right',
  },
});

export {LinkText};