import React, {Component} from 'react';
import {View, StyleSheet} from 'react-native';
import {BACKGROUND_CONTAINER_COLOR, PADDING_GENERAL_CONTAINER} from '../../utils/Constants';

class Container extends Component {
  render() {
    const {
      children,
      style = {},
    } = this.props;

    return (
      <View style={[styles.container, style]}>
        {children}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: BACKGROUND_CONTAINER_COLOR,
    padding: PADDING_GENERAL_CONTAINER,
  }
});

export {Container};