import React, {Component} from 'react';
import {View, Text, TextInput, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';

class InputText extends Component {
  state = {
    show: false,
  };

  toggleTypePassword = () => {
    const {show} = this.state;
    this.setState({show: !show});
  };

  render() {
    const {
      name,
      label,
      error,
      value,
      placeholder = '',
      secure = false,
      showPassword = false,
      autoCapitalize = 'sentences',
      onChange,
      onChangeText,
      style = {},
      multiline = false,
    } = this.props;

    const {show} = this.state;

    return (
      <View style={styles.container}>
        {label && <Text style={styles.label}>{label}</Text>}
        <TextInput
          name={name}
          clearTextOnFocus={false}
          style={[styles.input, showPassword ? styles.withSuffix : {}, style]}
          value={value}
          placeholder={placeholder}
          secureTextEntry={secure && !show}
          autoCapitalize={autoCapitalize}
          onChange={onChange}
          onChangeText={onChangeText}
          multiline={multiline}
          editable
        />
        {(secure && showPassword) && (
          <Icon
            onPress={this.toggleTypePassword}
            style={styles.suffix}
            name={show ? 'eye-off' : 'eye'}
            size={32}
          />
        )}
        {error && <Text style={styles.error}>{error}</Text>}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginBottom: 10,
  },
  input: {
    minHeight: 80,
    height: 'auto',
    borderWidth: 1,
    borderRadius: 5,
    borderColor: 'rgb(230, 235, 241)',
    paddingHorizontal: 26,
    fontSize: 21,
    color: 'rgb(67, 72, 77)',
  },
  withSuffix: {
    paddingRight: 65,
  },
  label: {
    fontSize: 16,
    marginBottom: 3,
    color: 'rgb(154, 171, 181)',
  },
  error: {
    color: 'red'
  },
  suffix: {
    position: 'absolute',
    right: 21,
    top: 24,
    color: 'rgb(154, 171, 181)'
  }
});

export {InputText};