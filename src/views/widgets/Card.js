import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';

class Card extends Component {
  render() {
    const {
      children,
      title,
      actions,
      style = {},
    } = this.props;

    return (
      <View style={[styles.container, style]}>
        {children}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    padding: 21,
    borderRadius: 5,
    elevation: 1,
    backgroundColor: '#fff'
  },
  // body: {
  //   padding: 15,
  // },
  // footer: {
  //   padding: 15,
  //   borderTopWidth: 1,
  //   borderTopColor: '#c9c9c9',
  // }
});

export {Card};