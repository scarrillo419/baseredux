import React, {Component} from 'react';
import {Text, StyleSheet} from 'react-native';
import {SECONDARY_COLOR} from '../../utils/Constants';

class SubtitleText extends Component {
  render() {
    const {
      text,
      style = {},
    } = this.props;

    return <Text style={[styles.text, style]}>{text}</Text>;
  }
}

const styles = StyleSheet.create({
  text: {
    fontSize: 24,
    // textAlign: 'center',
    lineHeight: 38,
    color: 'rgb(67, 72, 77)'
  }
});

export {SubtitleText};