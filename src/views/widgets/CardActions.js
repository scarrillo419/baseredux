import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';

class CardActions extends Component {
  render() {
    const {children} = this.props;

    return (
      <View style={styles.container}>
        {children}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    padding: 15,
    borderTopWidth: 1,
    borderTopColor: '#c9c9c9',
  },
});

export {CardActions};