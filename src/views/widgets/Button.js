import React, {Component} from 'react';
import {TouchableOpacity, View, Text, StyleSheet, ActivityIndicator} from 'react-native';
import {BUTTON_HEIGHT, DISABLED_COLOR, PRIMARY_COLOR, SECONDARY_COLOR, TEXT_BUTTON_COLOR} from '../../utils/Constants';

class Button extends Component {
  render() {
    const {
      onPress,
      title,
      loading,
      type = 'default',
      containerStyle = {},
      buttonStyle = {},
      textStyle = {},
      disabled = false,
    } = this.props;

    return (
      <View style={[styles.container, containerStyle]}>
        {loading
          ? <ActivityIndicator color={PRIMARY_COLOR} />
          : (
            <TouchableOpacity
              disabled={disabled}
              style={[styles.button, styles[disabled ? 'disabled': type], buttonStyle]}
              onPress={onPress}
            >
              <Text style={[styles[`${disabled ? 'disabled': type}Title`], textStyle]}>{title}</Text>
            </TouchableOpacity>
          )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: BUTTON_HEIGHT,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
  },
  button: {
    backgroundColor: '#fff',
    width: '100%',
    height: BUTTON_HEIGHT,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
  },
  defaultTitle: {
    fontSize: 21,
    color: SECONDARY_COLOR,
    fontWeight: 'bold',
  },
  primaryTitle: {
    fontSize: 21,
    color: '#fff',
    fontWeight: 'bold',
  },
  secondaryTitle: {
    fontSize: 21,
    color: '#fff',
    fontWeight: 'bold',
  },
  disabledTitle: {
    fontSize: 21,
    color: '#fff',
    fontWeight: 'bold',
  },
  primary: {
    backgroundColor: PRIMARY_COLOR,
    borderColor: PRIMARY_COLOR,
  },
  secondary: {
    backgroundColor: SECONDARY_COLOR,
    borderColor: SECONDARY_COLOR,
  },
  disabled: {
    backgroundColor: DISABLED_COLOR,
    borderColor: DISABLED_COLOR,
  },
});

export {Button};