import React, {Component} from 'react';
import {View, Text, StyleSheet, Dimensions, ScrollView, Image} from 'react-native';
import {BACKGROUND_CONTAINER_COLOR, PRIMARY_COLOR} from '../../utils/Constants';
import {TitleText, SubtitleText} from '../widgets';

const SCREEN_WIDTH = Dimensions.get('window').width;

class Slides extends Component {
  renderSlides = () => {
    const {data} = this.props;

    return data.map((slide, index) => (
      <View key={index} style={styles.slide}>
        <View>
          <Image
            source={slide.image}
            style={styles.image}
            resizeMode="stretch"
          />
        </View>
        <View style={styles.textContent}>
          <TitleText text={slide.title} />
          <SubtitleText text={slide.subtitle} style={{textAlign: 'center'}} />
        </View>
        <View style={styles.contentDots}>
          {data.map((dot, idx) => (
            <View key={idx} style={[styles.dot, styles[idx === slide.key ? 'current' : 'notCurrent']]} />
          ))}
        </View>
      </View>
    ));
  }

  render() {
    return (
      <ScrollView
        horizontal
        style={styles.container}
        pagingEnabled={true}
        showsHorizontalScrollIndicator={false}
      >
        {this.renderSlides()}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: BACKGROUND_CONTAINER_COLOR
  },
  slide: {
    flex: 1,
    width: SCREEN_WIDTH,
  },
  image: {
    width: SCREEN_WIDTH,
    height: SCREEN_WIDTH / 1.4,
  },
  textContent: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 40,
    // marginTop: 30,
  },
  contentDots: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: 30
  },
  dot: {
    width: 10,
    height: 10,
    marginHorizontal: 10,
    borderRadius: 10,
  },
  notCurrent: {
    backgroundColor: 'rgb(230, 235, 241)',
  },
  current: {
    backgroundColor: PRIMARY_COLOR,
  }
});

export default Slides;