import React, {Component} from 'react';
import {View, Text, Image, TouchableOpacity, StyleSheet} from 'react-native';
import {Card, SubtitleText, TitleText} from '../widgets';
import {DISABLED_COLOR, PRIMARY_COLOR} from '../../utils/Constants';

const teleMedicine = require('../../assets/images/teleMedicina.png');

class ServiceCard extends Component {
  render() {
    const {
      title,
      body,
      status,
    } = this.props;

    return (
      <Card style={{marginBottom: 15}}>
        <View style={styles.container}>
          <View style={styles.imageContainer}>
            <Image
              style={styles.image}
              source={teleMedicine}
            />
          </View>
          <View style={{flex: 1}}>
            <View style={styles.body}>
              <SubtitleText text={title} style={{fontWeight: 'bold'}} />
            </View>
            <View style={{marginTop: 15}}>
              <Text style={styles.textContainer}>
                <Text style={styles.textTitle}>Solicitado:</Text> {body.dateInit}
              </Text>
              {status === 'completed' && body.dateEnd && (
                <Text style={styles.textContainer}>
                  <Text style={styles.textTitle}>Finalizado:</Text> {body.dateEnd}
                </Text>
              )}
              {(status === 'inProgress' || status === 'completed') && body.attendedBy && (
                <Text style={styles.textContainer}>
                  <Text style={styles.textTitle}>Atendido por:</Text> {body.attendedBy}
                </Text>
              )}
              <Text style={styles.textContainer}>
                <Text style={styles.textTitle}>Sintomas:</Text> {body.symptoms}
              </Text>
            </View>
          </View>
        </View>
        <View style={[styles.footer, {justifyContent:  status === 'completed' ? 'space-between' : 'flex-end'}]}>
          {status === 'completed' && (
            <TouchableOpacity>
              <Text style={styles.footerAction}>Calificar servicio</Text>
            </TouchableOpacity>
          )}
          <TouchableOpacity>
            <Text style={styles.footerAction}>Ver detalle</Text>
          </TouchableOpacity>
        </View>
      </Card>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  imageContainer: {
    paddingRight: 20,
  },
  image: {
    width: 53,
    height: 53,
  },
  body: {
    borderBottomWidth: 1,
    borderBottomColor: DISABLED_COLOR,
    paddingBottom: 15
  },
  textContainer: {
    fontSize: 16,
    lineHeight: 30,
    color: 'rgb(67, 72, 77)',
  },
  textTitle: {
    color: 'rgb(154, 171, 181)',
  },
  footer: {
    paddingTop: 15,
    borderTopWidth: 1,
    borderTopColor: DISABLED_COLOR,
    marginTop: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  footerAction: {
    color: PRIMARY_COLOR,
    fontSize: 18,
    lineHeight: 32,
  },
});

export default ServiceCard;