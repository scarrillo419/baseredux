import React, {Component} from 'react';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';
import {TitleText} from '../widgets';
import {PRIMARY_COLOR} from '../../utils/Constants';

class AppHeader extends Component {
  render () {
    const {navigation} = this.props;

    return (
      <View style={styles.container}>
        <TitleText text="Hola Usuario" />
        <View style={styles.subtitles}>
          <Text style={styles.text}>Role</Text>
          <TouchableOpacity onPress={() => navigation.navigate('RequestService')}>
            <Text style={[styles.text, styles.action]}>Solicitar servicio</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingTop: 40,
    paddingHorizontal: 21,
    backgroundColor: '#fff'
  },
  subtitles: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 40,
  },
  text: {
    fontSize: 18,
  },
  action: {
    color: PRIMARY_COLOR,
  }
});

export default AppHeader;