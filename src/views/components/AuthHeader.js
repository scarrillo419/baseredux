import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import Icon  from 'react-native-vector-icons/Feather';
import {BACKGROUND_CONTAINER_COLOR, SECONDARY_COLOR} from '../../utils/Constants';

class AuthHeader extends Component {
  render() {
    const {navigation, title} = this.props;

    return (
      <View style={styles.container}>
        <View>
          <TouchableOpacity onPress={() => navigation.goBack()}>
            <Icon
              name="arrow-left"
              size={32}
              color={SECONDARY_COLOR}
            />
          </TouchableOpacity>
        </View>
        <View>
          <Text style={styles.title}>{title}</Text>
        </View>
        <View>
          <TouchableOpacity>
            <Icon
              name="info"
              size={32}
              color={SECONDARY_COLOR}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: BACKGROUND_CONTAINER_COLOR,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 21,
    borderBottomWidth: 1,
    borderBottomColor: 'rgb(230, 235, 241)'
  },
  title: {
    fontSize: 21,
    color: SECONDARY_COLOR,
    lineHeight: 35
  }
});

export default AuthHeader;