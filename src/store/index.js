import {combineReducers} from 'redux';
import { all, fork } from 'redux-saga/effects';

import authReducer from './auth/reducers';
import {authSaga} from './auth/sagas';

export const rootReducer = combineReducers({
  auth: authReducer,
});

export function* rootSaga() {
  yield all([
    fork(authSaga)
  ]);
}
