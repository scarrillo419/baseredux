import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import reduxSaga from 'redux-saga';
import {persistStore} from 'redux-persist';
import {rootReducer, rootSaga} from './index';

export default function configureStore() {
  const sagaMiddleware = reduxSaga();
  const store = createStore(
    rootReducer,
    applyMiddleware(sagaMiddleware, thunk)
  );
  sagaMiddleware.run(rootSaga);

  const persistor = persistStore(store);

  return {
    store,
    persistor,
  };
}
