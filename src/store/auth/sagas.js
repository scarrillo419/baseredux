import {all, fork, takeEvery, put} from 'redux-saga/effects';
import apis from './apis';
import {AUTH_SIGNIN} from './types';
import {errorRequest, setCurrentUser, successRequest} from './actions';

function* signin() {
  yield takeEvery(AUTH_SIGNIN, function* (action) {
    try {
      console.log('submitting...');
      const data = yield apis.signin(action.payload);
      console.log('data: ', data);
      // yield put(setCurrentUser(data));
      yield put(successRequest());
    } catch (error) {
      console.log('error: ', JSON.stringify(error.response));
      yield put(errorRequest(error));
    }
  });
}

export function* authSaga() {
  yield all([
    fork(signin),
  ]);
}
