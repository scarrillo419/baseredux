import {User} from '../../models/User';
import {
  AUTH_ERROR_REQUEST,
  AUTH_RESEND_CODE,
  AUTH_SIGNIN,
  AUTH_SIGNOUT,
  AUTH_SIGNUP,
  AUTH_SUCCESS_REQUEST,
  AUTH_VALIDATE_CODE,
} from './types';

const initialState = {
  user: new User({}),
  authenticated: false,
  token: '',
  error: '',
  message: '',
  loading: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case AUTH_SIGNIN:
    case AUTH_SIGNUP:
    case AUTH_SIGNOUT:
    case AUTH_VALIDATE_CODE:
    case AUTH_RESEND_CODE:
      return {
        ...state,
        loading: true,
      };
    case AUTH_SUCCESS_REQUEST:
      return {
        ...state,
        loading: false,
        ...(action.payload) && {
          message: action.payload
        }
      }
    case AUTH_ERROR_REQUEST:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    default:
      return state;
  }
};