import {
  AUTH_ERROR_REQUEST,
  AUTH_RESEND_CODE,
  AUTH_SET_CURRENT_USER,
  AUTH_SIGNIN,
  AUTH_SIGNUP,
  AUTH_SUCCESS_REQUEST,
  AUTH_VALIDATE_CODE,
} from './types';

export const signin = () => dispatch => {
  dispatch({type: AUTH_SIGNIN});
};

export const signup = () => dispatch => {
  dispatch({type: AUTH_SIGNUP});
};

export const validateCode = () => dispatch => {
  dispatch({type: AUTH_VALIDATE_CODE});
};

export const resendCode = () => dispatch => {
  dispatch({type: AUTH_RESEND_CODE});
};

export const setCurrentUser = (data) => dispatch => {
  dispatch({
    type: AUTH_SET_CURRENT_USER,
    payload: data,
  });
};

export const successRequest = (data) => dispatch => {
  dispatch({
    type: AUTH_SUCCESS_REQUEST,
    payload: data,
  });
};

export const errorRequest = (data) => dispatch => {
  dispatch({
    type: AUTH_ERROR_REQUEST,
    payload: data,
  });
};
