import axios from 'axios';

// const apiUrl = 'http://192.168.1.5:3005';
const apiUrl = 'https://venemed.vidaplatform.com/api';

export default {
  async signin(payload) {
    const {data} = await axios.post(`${apiUrl}/v1/clients/sign-in`, payload);
    return data;
  },
  async signup(payload) {
    const {data} = await axios.post(`${apiUrl}/v1/clients/sign-up`, payload);
    return data;
  },
  async validateCode(payload) {
    const {data} = await axios.post(`${apiUrl}/code-validation`, payload);
    return data;
  },
  async resendCode(payload) {
    const {data} = await axios.post(`${apiUrl}/resend-code`, payload);
    return data;
  },
};
