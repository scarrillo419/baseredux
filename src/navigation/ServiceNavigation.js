import React, {Component} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import NotAffiliateScreen from '../views/screens/services/NotAffiliateScreen';
import DashboardScreen from '../views/screens/services/DashboardScreen';
import AppHeader from '../views/components/AppHeader';
import RequestServiceScreen from '../views/screens/services/RequestServiceScreen';
import TelemedicineScreen from '../views/screens/services/TelemedicineScreen';
import {SECONDARY_COLOR} from '../utils/Constants';
import Icon from 'react-native-vector-icons/Feather';

const Stack = createStackNavigator();

class ServiceNavigation extends Component {
  render() {
    return (
      <Stack.Navigator>
        <Stack.Screen
          options={{header: () => null}}
          name="Welcome"
          component={NotAffiliateScreen}
        />
        <Stack.Screen
          options={{header: ({navigation}) => <AppHeader navigation={navigation} />}}
          name="Dashboard"
          component={DashboardScreen}
        />
        <Stack.Screen
          options={{
            title: 'Solicitar servicio',
            headerTintColor: SECONDARY_COLOR,
            headerTitleAlign: 'center',
            headerStyle: {
              height: 70,
            },
          }}
          name="RequestService"
          component={RequestServiceScreen}
        />
        <Stack.Screen
          options={{
            title: 'Telemedicina',
            headerTintColor: SECONDARY_COLOR,
            headerTitleAlign: 'center',
            headerStyle: {
              height: 70,
            },
            headerLeftContainerStyle: {
              marginLeft: 10,
            },
            headerRightContainerStyle: {
              marginRight: 21
            },
            headerRight: () => (
              <Icon
                name="info"
                size={32}
                color={SECONDARY_COLOR}
              />
            )
          }}
          name="Telemedicine"
          component={TelemedicineScreen}
        />
      </Stack.Navigator>
    );
  }
}

export default ServiceNavigation;