import React, {Component} from 'react';
import {View, Text, Image, StyleSheet} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import ServiceNavigation from './ServiceNavigation';

const pinActive = require('../assets/images/pinActive.png');
const pinInactive = require('../assets/images/pinInactive.png');
const beneficiaryActive = require('../assets/images/beneficiaryActive.png');
const beneficiaryInactive = require('../assets/images/beneficiaryInactive.png');
const profileActive = require('../assets/images/profileActive.png');
const profileInactive = require('../assets/images/profileInactive.png');

const Tab = createBottomTabNavigator();

const BeneficiaresScreen = () => (
  <View>
    <Text>Beneficiary</Text>
  </View>
);

const ProfileScreen = () => (
  <View>
    <Text>Profile</Text>
  </View>
);

class AppNavigation extends Component {
  render() {
    return (
      <Tab.Navigator
        screenOptions={({route}) => ({
          tabBarIcon: ({focused}) => {
            let image;
            if (route.name === 'Services') {
              image = focused ? pinActive : pinInactive;
            } else if (route.name === 'Beneficiary') {
              image = focused ? beneficiaryActive : beneficiaryInactive;
            } else {
              image = focused ? profileActive : profileInactive;
            }
            return <Image style={styles.image} source={image} />
          },
        })}
        tabBarOptions={{
          activeTintColor: 'rgb(251, 49, 74)',
          inactiveTintColor: 'gray',
          labelStyle: {
            marginBottom: 12,
            marginTop: 0,
            paddingTop: 0,
            fontSize: 14,
          },
          style: {
            height: 80,
            alignItems: 'center',
            justifyContent: 'center'
          }
        }}
      >
        <Tab.Screen
          name="Services"
          component={ServiceNavigation}
        />
        <Tab.Screen
          name="Beneficiary"
          component={BeneficiaresScreen}
        />
        <Tab.Screen
          name="Profile"
          component={ProfileScreen}
        />
      </Tab.Navigator>
    );
  }
}

const styles = StyleSheet.create({
  image: {
    width: 35,
    height: 35,
  }
});

export default AppNavigation;