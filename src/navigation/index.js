import {createRef} from 'react';

export const isMountedRef = createRef();
export const navigationRef = createRef();

export function navigate(name, params) {
  if (isMountedRef.current && navigationRef.current) {
    navigationRef.current.navigate(name, params);
  }
}