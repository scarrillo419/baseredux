import React, {Component} from 'react';
import {connect} from 'react-redux';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import SigninScreen from '../views/screens/auth/SigninScreen';
import WelcomeScreen from '../views/screens/WelcomeScreen';
import AuthHeader from '../views/components/AuthHeader';
import RestorePasswordScreen from '../views/screens/auth/RestorePasswordScreen';
import RestoreSuccessScreen from '../views/screens/auth/RestoreSuccessScreen';
import SignupScreen from '../views/screens/auth/SignupScreen';
import AppNavigation from './AppNavigation';

const Stack = createStackNavigator();

class RootNavigation extends Component {
  componentDidMount() {
    console.log('here...');
  }

  renderHeader = (navigation, title) => (
    <AuthHeader navigation={navigation} title={title} />
  );

  render() {
    const {navigationRef} = this.props;

    return (
      <NavigationContainer ref={navigationRef}>
        <Stack.Navigator>
          <Stack.Screen
            options={{header: () => null}}
            name="Welcome"
            component={WelcomeScreen}
          />
          <Stack.Screen
            options={{
              header: ({navigation}) => this.renderHeader(navigation, 'Ya eres usuario'),
            }}
            name="Signin"
            component={SigninScreen}
          />
          <Stack.Screen
            options={{
              header: ({navigation}) => this.renderHeader(navigation, 'Eres un usuario nuevo'),
            }}
            name="Signup"
            component={SignupScreen}
          />
          <Stack.Screen
            options={{
              header: ({navigation}) => this.renderHeader(navigation, '¿Olvidaste tu contraseña?'),
            }}
            name="Restore"
            component={RestorePasswordScreen}
          />
          <Stack.Screen
            options={{header: () => null}}
            name="RestoreSuccess"
            component={RestoreSuccessScreen}
          />
          <Stack.Screen
            options={{header: () => null}}
            name="App"
            component={AppNavigation}
          />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}

export default connect(null, null)(RootNavigation);