export class User {
  constructor(info: any) {
    this._id = info._id;
    this.email = info.email;
    this.name = info.name;
    this.lastName = info.lastName;
    this.avatar = info.avatar;
    this.status = info.status || 'active';
  }
}