export const PRIMARY_COLOR = 'rgb(251, 49, 74)';
export const SECONDARY_COLOR = 'rgb(32, 8, 68)';
export const DISABLED_COLOR = 'rgb(230, 235, 241)';
export const LINK_COLOR = 'rgb(251, 49, 74)';
export const TEXT_BUTTON_COLOR = 'rgb(255, 255, 255)';
export const BACKGROUND_CONTAINER_COLOR = 'rgb(255, 255, 255)';
export const PADDING_GENERAL_CONTAINER = 20;
export const BUTTON_HEIGHT = 80;