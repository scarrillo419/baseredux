import * as yup from 'yup';

const SigninValidation = yup.object().shape({
  email: yup
    .string()
    // .email({email: 'El formato de correo es inválido'})
    .required({email: 'Este campo es requerido'}),
  password: yup
    .string()
    .required({password: 'Este campo es requerido'}),
});

export {SigninValidation};