import * as yup from 'yup';

function equalTo(ref, msg) {
  return this.test({
    name: 'equalTo',
    exclusive: false,
    message: msg,
    params: { reference: ref.path },
    test: function(value) {
      return value === this.resolve(ref)
    }
  })
}

yup.addMethod(yup.string, 'equalTo', equalTo);

const SignupValidation = yup.object().shape({
  email: yup
    .string()
    // .email({email: 'El formato de correo es inválido'})
    .required({email: 'Este campo es requerido'}),
  password: yup
    .string()
    .required({password: 'Este campo es requerido'}),
  confirmPassword: yup
    .string()
    .equalTo(yup.ref('password'), {confirmPassword: 'La contraseña de confirmación no coincide'})
    .required({password: 'Este campo es requerido'}),
  policy: yup.bool().oneOf([true]),
});

export {SignupValidation};