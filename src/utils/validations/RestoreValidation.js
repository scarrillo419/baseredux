import * as yup from 'yup';

const RestoreValidation = yup.object().shape({
  email: yup
    .string()
    .email({email: 'El formato de correo es inválido'})
    .required({email: 'Este campo es requerido'}),
});

export {RestoreValidation};