import 'react-native-gesture-handler';
import React, {Component} from 'react';
import {Text} from 'react-native';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';

import {isMountedRef, navigationRef} from './src/navigation';
import RootNavigation from './src/navigation/RootNavigation';
import configureStore from './src/store/configureStore';

const {store, persistor}  = configureStore();

class App extends Component {
  componentDidMount() {
    console.log('app.js....');
    isMountedRef.current = true;

    return () => (isMountedRef.current = false);
  }

  render() {
    return (
      <Provider store={store}>
        {/*<PersistGate loading={null} persistor={persistor}>*/}
          <RootNavigation navigationRef={navigationRef}/>
        {/*</PersistGate>*/}
      </Provider>
    );
  }
}

export default App;
